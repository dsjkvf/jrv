" Syntax
hi! def link CursorLine Pmenu
syntax match  jrvURL            /\thttp.*$/ conceal
syntax match  jrvOLD            /^_/        conceal
syntax region jrvTitleOld       start=/^_.*/  end=/\thttp.*$/ contains=jrvOLD,jrvURL keepend
syntax region jrvTitle          start=/^[^_].*/    end=/\thttp.*$/ contains=jrvURL keepend
hi! def link jrvTitle          Question
hi! def link jrvTitleOld       String

" Options
set concealcursor=nv
set conceallevel=3
set cursorline
set laststatus=0
set nolist
set number
set showtabline=0
set tabstop=8

" Mappings
" open
" nnoremap <buffer> <silent> <CR> :call system('tmux popup -xC -yC -w150 -h50 -s bg="#0b0b45" -S bg="#0b0b45" -E "reddio.sh comments/' . getline('.')->matchstr("http.*")->split('/')[-1] . ' withlinks \| less -+F -R"')<CR>
nnoremap <buffer> <silent> <CR> :call system('tmux new-window "reddio.sh comments/' . getline('.')->matchstr("http.*")->split('/')[-1] . ' withlinks \| less -+F -R"')<CR>
" navigate
nnoremap <buffer> <silent> <Space> j!<CR>
" quit
nnoremap <buffer> <silent> q :qa!<CR>
